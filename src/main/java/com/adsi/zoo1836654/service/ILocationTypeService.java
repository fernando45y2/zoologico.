package com.adsi.zoo1836654.service;

import com.adsi.zoo1836654.domian.LocationType;
import org.springframework.http.ResponseEntity;

public interface ILocationTypeService {

    public LocationType create(LocationType locationType);

    public Iterable<LocationType> read();
}
