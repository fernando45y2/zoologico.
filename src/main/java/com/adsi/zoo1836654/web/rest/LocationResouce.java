package com.adsi.zoo1836654.web.rest;

import com.adsi.zoo1836654.domian.Location;
import com.adsi.zoo1836654.service.ILocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/location")
public class LocationResouce {

    @Autowired
    ILocationService locationService;

    @PostMapping("")
    public Location create(@RequestBody Location location){
        return locationService.create(location);
    }

    @GetMapping("")
    public Iterable<Location> read(){
        return locationService.read();
    }

}
